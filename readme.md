# Scroll Effects

Animate stuff while you scroll.

### Installation

Install via Bower

```bash
bower install https://bitbucket.org/relishinc/jquery-scroll-effects.git --save
```

Include the JS and CSS files
```html
<script src="jquery.scroll-effects.min.js"></script>
<link rel="stylesheet" href="jquery.scroll-effects.min.css">
```

You'll also need jQuery and [GSAP's TweenMax](https://greensock.com/tweenmax)

```html
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.2/TweenMax.min.js"></script>
```

### Usage

Set up your elements:

```html
<div data-scroll='{ "y": 100, "opacity": 0 }' data-scroll-start="0" data-scroll-end="0.5">
    Animate me!
</div>
```
Available attributes are:

`data-scroll` *(required)* is a valid JSON object containing the [CSS properties](https://greensock.com/docs/Plugins/CSSPlugin) you want to animate (i.e. the "initial" state of your animation). If you want to animate *to* a state, use `data-scroll-to` instead
`data-scroll-start` at what point the animation should start: 0 is when the element starts to enter the viewport and 1 is when it starts to leave (default: *0*)
`data-scroll-end` when the animation should end (default: *1*)
`data-scroll-exit="true"` adjusts the animation timeline so that `data-scroll-end="1"` is calculated based on when the element has *completely left* the viewport

Enable the scrolling effects in JS:

```javascript
$(document).ready(function() {
   SCROLL.init(); 
});
```

### Options

There are no options!