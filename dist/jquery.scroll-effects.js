/* Name: jquery.scroll-effects */
/* Version: 0.0.4 */
/* Generated Tue, 24 Dec 2019 00:29:08 GMT */
"use strict";

var SCROLL = function () {
  var _init = function _init($options) {
    SCROLL.options = $options || {};
    var items = [];
    $('[data-scroll], [data-scroll-from], [data-scroll-to]').each(function (index, el) {
      //<div 
      //	data-scroll-from='{ "y": 100, "opacity": 0 }'	 // the properties to animate (starting values)
      // 	data-scroll-start="0"														// when to start animation [ 0 = when element STARTS to enter viewport ]
      //	data-scroll-end="1"															// when to stop animation [ 1 = when element STARTS to leave viewport ]
      // 	data-scroll-exit="true"													// calculate "end" based on when element completely LEAVES viewport
      //>
      try {
        var tl = new TimelineLite();

        if ($(el).data('scroll-to')) {
          // clear out any old inline styles
          for (var prop in $(el).data('scroll-to') || {}) {
            el.style[prop] = null;
          }

          tl.to($(el), 1, $(el).data('scroll-to'));
        } else {
          // clear out any old inline styles
          for (var prop in $(el).data('scroll') || $(el).data('scroll-from') || {}) {
            el.style[prop] = null;
          }

          tl.from($(el), 1, $(el).data('scroll') || $(el).data('scroll-from'));
        }

        tl.pause(); //$(el).css({ transform: 'translate3d(0,0,0)', backfaceVisibility: 'hidden' })

        items.push({
          el: el,
          tl: tl
        });
      } catch (e) {
        console.log('Could not animate on scroll:', e);
      }
    });
    var ticking = false;

    var update = function update() {
      var wh = $(window).height(),
          st = $(window).scrollTop();
      $.each(items, function (index, item) {
        var et = $(item.el).offset().top,
            eh = $(item.el).outerHeight(),
            end = $.isNumeric($(item.el).data('scroll-end')) ? $(item.el).data('scroll-end') : 1,
            start = $.isNumeric($(item.el).data('scroll-start')) ? $(item.el).data('scroll-start') : 0,
            enter = et + wh * (start - 1),
            // when element enters "start" point
        exit = $(item.el).data('scroll-exit') ? // when element leaves "end" point 
        et + eh + wh * (end - 1) : // element completely leaves viewport
        et + wh * (end - 1); // element starts to leave viewport

        var progress = Math.max(0, Math.min(1, (st - enter) / (exit - enter)));
        item.tl.progress(progress);
      });
      ticking = false;
    };

    var requestTick = function requestTick() {
      if (!ticking) {
        window.requestAnimationFrame(update);
        ticking = true;
      }
    };

    var onScroll = function onScroll() {
      requestTick();
    };

    $(window).on('scroll.scroll resize.scroll', function (e) {
      return onScroll();
    });
  };

  return {
    init: _init
  };
}(); //export { SCROLL as default };